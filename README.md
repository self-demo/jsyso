### jsyso：基于Spring JDBC的最简开发框架

我们身处于ORM繁荣时代，经历无数ORM框架的规则给我们带来便利之后，我们都无意识的追逐原始sql给我们带来的自由。jsyso也是其中一位，带着这种执着和信心试图以最简单的方式诠释高效Dao开发。

### insert：插入

```java
Dao.get("sys_user").insert(JsMap.create("id", 2)	// id
		.set("login_name", "jadmin")				// 登录名
		.set("name", "jadmin")						// 昵称
		.set("password", "123456")					// 密码
		.set("create_date", new Date())				// 创建时间
		.set("update_date", new Date())				// 更新时间
		.set("del_flag", 0));						// 是否删除
```

### insertAll：批量插入

```java
// 批量插入10个用户
List<JsMap> list = new ArrayList<JsMap>();
for(int i=1; i<=10; ++i) {
	list.add(JsMap.create("id", i + 2)		// id
			.set("login_name", "jadmin" + i)// 登录名
			.set("name", "jadmin" + i)		// 昵称
			.set("password", "123456")		// 密码
			.set("create_date", new Date())	// 创建时间
			.set("update_date", new Date())	// 更新时间
			.set("del_flag", 0)				// 是否删除
			);
}
int result = Dao.get("sys_user").insertAll(list);
// 输出：10
System.out.println(result);
```

### select：列表查询

```java
// like查询登录名是jadmin前缀的
List<JsMap> userList = Dao.get("sys_user").select(JsMap.create("login_name", new String[]{
		"like", "'jadmin%'"
}));
for(JsMap user : userList) {
	System.out.println(user);
}
// 等价于==
userList = Dao.get("sys_user").where(Wheres.create().like("login_name", "'jadmin%'")).select();
for(JsMap user : userList) {
	System.out.println(user);
}
```

### find：查询单个

```java
// 查询登录名是：jadmin的用户
JsMap user = Dao.get("sys_user").find(JsMap.create("login_name", "jadmin"));
System.out.println(user);	
```

### update：更新数据

```java
// 把id=2，login_name=jadmin密码更新成：123456
Dao.get("sys_user").update(JsMap.create("id", 2).set("password", "123456"));
// 约等价于≈=
Dao.get("sys_user").where(Wheres.create("login_name", "jadmin")).update(JsMap.create("password", "123456"));
```

### delete：删除数据

```java
// 物理删除id=3，login_nam=jadmin1用户
Dao.get("sys_user").delete(JsMap.create("id", 3));
```

### 借鉴感谢

- ThinkPHP（Dao设计参考）
- fastjson（JsMap、CastUtils设计参考）

### 完整案例：jadmin

**jadmin**是基于jsyso + LayUI搭建的后台管理系统，目前已自带权限控制模块。

[https://git.oschina.net/xujian_jason/jadmin](https://git.oschina.net/xujian_jason/jadmin)

### 更多问题

更多问题欢迎QQ加好友：514737546（加时还请说明(～￣▽￣)～）