package com.jsyso.jsyso.cache;

import org.junit.Test;

public class LazyCacheTest {

	private static Cache cache = new LazyCache();
	
	@Test
	public void test() throws InterruptedException {
		Element e = Element.create("943a3e7e8f83a8ec9e2f5f99a5689cf9", "access_token=IEBeR4V7ru4qTVV2HrHUq1t7dTV6kKA9YMWgRiciKgS49KapPYWCyQtJVx8VgcqWymiaXiS1uyrpKFCjJ2T5gmNg3gGEJ1heQZxrTfmsIpmLT1ppBJJsQ0O1xa0QuuWfIUQaAGALXT")
		.setSeconds(6);
		cache.put(e);
		Thread.sleep(5000);
		String token = cache.get(e);
		System.out.println(token);
		Thread.sleep(2000);
		token = cache.get(e);
		System.out.println(token);
	}
	
}
