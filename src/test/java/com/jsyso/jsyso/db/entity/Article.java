package com.jsyso.jsyso.db.entity;

public class Article implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	public static final int DEL_FLAG_YES = 1;

	
	private Long id;
	private String title;
	private Integer delFlag;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(Integer delFlag) {
		this.delFlag = delFlag;
	}

}
