package com.jsyso.jsyso.db;

import org.junit.Test;

public class PageTest {
	@Test
	public void template1() {
		Page page = new Page(5, 54, 5);
		page.setTplBegin("<div class=\"pagination\" id=\"pagination\">\n");
		page.setTplItem("<a href=\"javascript:{funcName};\" class=\"item{active}\">{i}</a>\n");
		page.setTplEnd("</div>");
		System.out.println(page.show());
	}
}
