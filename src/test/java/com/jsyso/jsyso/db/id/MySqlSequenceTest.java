package com.jsyso.jsyso.db.id;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring-context.xml"}) 
public class MySqlSequenceTest {

	@Autowired
	private Sequence seq;
	
	@Test
	public void getNextId() {
		Long id = seq.nextVal("test_cms_sequence");
		System.out.println(id);
		id = seq.nextVal("test_cms_sequence");
		System.out.println(id);
	}
	
}
