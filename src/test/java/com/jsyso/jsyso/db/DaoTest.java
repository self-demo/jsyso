package com.jsyso.jsyso.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jsyso.jsyso.util.JsMap;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring-context.xml"}) 
public class DaoTest {

	@Test
	public void testSelect() {
//		JsMap map = JsMap.create("del_flag", 1);
//		Dao dao = Dao.get("cms_article", "id");
//		List<JsMap> result = dao.field("id, title")
//				.where(Wheres.create(map.keySet())).select(map);
//		System.out.println(result);
//		System.out.println("\n==================================\n");
//		// like查询登录名是jadmin前缀的
//		List<JsMap> userList = Dao.get("sys_user").select(JsMap.create("login_name", new String[]{
//				"like", "'jadmin%'"
//		}));
//		for(JsMap user : userList) {
//			System.out.println(user);
//		}
//		// 等价于==
//		userList = Dao.get("sys_user").where(Wheres.create().like("login_name", "'jadmin%'")).select();
//		for(JsMap user : userList) {
//			System.out.println(user);
//		}
	}
	
	@Test
	public void testFind() {
//		JsMap map = JsMap.create("del_flag", 1);
//		Dao dao = Dao.get("cms_article");
//		JsMap result = dao.where(Wheres.create(map.keySet())).find(map);
//		System.out.println(result);
//		System.out.println("\n==================================\n");
		// 查询登录名是：jadmin的用户
//		JsMap user = Dao.get("sys_user").find(JsMap.create("login_name", "jadmin"));
//		System.out.println(user);
	}
	
	@Test
	public void testInsert() {
//		JsMap map = JsMap.create("id", "1")
//				.set("title", "test")
//				.set("create_date", new java.util.Date())
//				.set("del_flag", 0);
//		int result = Dao.get("test_cms_article").insert(map);
//		System.out.println(result);
//		Dao.get("sys_user").insert(JsMap.create("id", 2)	// id
//		.set("login_name", "jadmin")				// 登录名
//		.set("name", "jadmin")						// 昵称
//		.set("password", "123456")					// 密码
//		.set("create_date", new Date())				// 创建时间
//		.set("update_date", new Date())				// 更新时间
//		.set("del_flag", 0));						// 是否删除
	}
	
	@Test
	public void testReplace() {
//		JsMap map = JsMap.create("id", "1")
//				.set("title", "replace")
//				.set("create_date", new java.util.Date())
//				.set("del_flag", 1);
//		int result = Dao.get("test_cms_article").insert(map, true);
//		System.out.println(result);
	}
	
	@Test
	public void testInsertAll() {
//		// 批量插入10个用户
//		List<JsMap> list = new ArrayList<JsMap>();
//		for(int i=1; i<=10; ++i) {
//			list.add(JsMap.create("id", i + 2)		// id
//					.set("login_name", "jadmin" + i)// 登录名
//					.set("name", "jadmin" + i)		// 昵称
//					.set("password", "123456")		// 密码
//					.set("create_date", new Date())	// 创建时间
//					.set("update_date", new Date())	// 更新时间
//					.set("del_flag", 0)				// 是否删除
//					);
//		}
//		int result = Dao.get("sys_user").insertAll(list);
//		// 输出：10
//		System.out.println(result);
	}
	
	@Test
	public void testUpdate() {
//		JsMap map = JsMap.create("id", "3")
//		.set("title", "update")
//		.set("create_date", new java.util.Date())
//		.set("del_flag", 1);
//		int result = Dao.get("test_cms_article").update(map);
//		System.out.println(result);
		// 把id=2，login_name=jadmin密码更新成：123456
//		Dao.get("sys_user").update(JsMap.create("id", 2).set("password", "123456"));
//		// 约等价于≈=
//		Dao.get("sys_user").where(Wheres.create("login_name", "jadmin")).update(JsMap.create("password", "123456"));
	}
	
	@Test
	public void testCount() {
//		JsMap map = JsMap.create()
//		.set("del_flag", 0);
//		long result = Dao.get("test_cms_article").count(map);
//		System.out.println(result);
	}

	@Test
	public void testPage() {
//		JsMap map = JsMap.create()
//				.set("del_flag", 0);
//		List<JsMap> articles = Dao.get("test_cms_article")
//				.order("id DESC")
//				.page(2, 3).select(map);
//		System.out.println(articles);
	}
	
	@Test
	public void testInsertEntity() {
//		Article article = new Article();
//		article.setTitle("entity insert");
//		article.setDelFlag(0);
//		JsMap map = Maps.beanToJsMap(article)
//				.set("create_date", new Date());
//		int result = Dao.get("test_cms_article").insert(map);
//		System.out.println(result);
	}
	
	@Test
	public void testSelectLastid() {
//		JsMap map = JsMap.create("del_flag", 0);
//		Dao dao = Dao.get("test_cms_article");
//		List<JsMap> result = dao.field("id, title")
//				.where(Wheres.create(map.keySet()).elt("id", 5)).select(map);
//		System.out.println(result);
	}
	
	@Test
	public void testDelete() {
//		// 物理删除id=3，login_nam=jadmin1用户
//		Dao.get("sys_user").delete(JsMap.create("id", 3));
	}
	
}
