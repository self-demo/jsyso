package com.jsyso.jsyso.util.jcollect;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.jsyso.jsyso.util.Callback;

public class JCollectTest {

	@Test
	public void test1() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("a", "a");
		map.put("b", "b");
		map.put("c", 1);
		map.put("d", 12.34f);
		Map<String, Object> map1 = JCollect.begin(map).exclude("a", "b").handle(new Callback<Object, Object>() {
			public Object handle(Object param) {
				return 2;
			}
		}, "c").end();
		System.out.println(map1);
	}
	
}
