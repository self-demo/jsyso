package com.jsyso.jsyso.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.jsyso.jsyso.lang.JStringUtils;

public class MapsTest {

//	@Test
	public void testFilter() {
		Map<String, String> sqlMap = new HashMap<String, String>();
		sqlMap.put("base", "id, del_flag AS delFlag");
		sqlMap.put("content", "title");
		sqlMap.put("date", "create_date, update_date");
		System.out.println(StringUtils.join(Maps.filter(sqlMap, new Filter<String>() {
			@Override
			public boolean apply(String t) {
				return JStringUtils.equalsAny(t, "base", "content");
			}
		}).values(), ", "));
	}
	
	@Test
	public void testJsMap() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
//		ClassLoader loader = Thread.currentThread().getContextClassLoader();
//        final JsMap obj = JsMap.create();
//        Object proxy = Proxy.newProxyInstance(loader, new Class<?>[] { Map.class }, obj);
//        System.out.println(proxy);
		final JsMap map = JsMap.create("id", 1).set("name", "jan");
		Object myBean = map.getBean();
		Method getter = myBean.getClass().getMethod("getId");
		System.out.println(getter.invoke(myBean));
		getter = myBean.getClass().getMethod("getName");
		System.out.println(getter.invoke(myBean));
	}
}
