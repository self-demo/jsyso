package com.jsyso.jsyso.lang;

import java.util.Iterator;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import com.jsyso.jsyso.util.Callback;
import com.jsyso.jsyso.util.Filter;

public class JStringUtils {

	public static final char DEFAULT_CAMEL_SEPARATOR = '_';
	
	/**
	 * 等于其中一项则返回true
	 * @param cs
	 * @param css
	 * @return
	 */
	public static boolean equalsAny(final CharSequence cs, final CharSequence... css) {
		if(StringUtils.isEmpty(cs) || ArrayUtils.isEmpty(css)) {
			return false;
		}
		for(CharSequence cur : css) {
			if(StringUtils.equals(cs, cur)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 驼峰命名法工具
	 * toCamelCase("hello_world", '_') == "helloWorld" 
	 */
    public static String toCamelCase(String s, char separator) {
        if (StringUtils.isEmpty(s)) {
            return null;
        }
        s = s.toLowerCase();
        StringBuilder sb = new StringBuilder(s.length());
        boolean upperCase = false;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == separator) {
                upperCase = true;
            } else if (upperCase) {
                sb.append(Character.toUpperCase(c));
                upperCase = false;
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }
    
    /**
   	 * 驼峰命名法工具
   	 * toUnderScoreCase("helloWorld", '_') = "hello_world"
   	 */
	public static String toUnderScoreCase(String s, char separator) {
		if (StringUtils.isEmpty(s)) {
			return null;
		}
		StringBuilder sb = new StringBuilder();
		boolean upperCase = false;
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			boolean nextUpperCase = true;
			if (i < (s.length() - 1)) {
				nextUpperCase = Character.isUpperCase(s.charAt(i + 1));
			}
			if ((i > 0) && Character.isUpperCase(c)) {
				if (!upperCase || !nextUpperCase) {
					sb.append(separator);
				}
				upperCase = true;
			} else {
				upperCase = false;
			}
			sb.append(Character.toLowerCase(c));
		}
		return sb.toString();
	}
	

	/**
	 * 首字母小写
	 * @param name
	 * @return
	 */
	public static String firstLower(String name) {
		if(StringUtils.isBlank(name)) {
			return null;
		}
		char[] carr = name.toCharArray();
		if(carr.length == 0) {
			return null;
		}
		carr[0] += 32; // 转小写
		return String.valueOf(carr);
	}
	
	/**
	 * 拼接数组
	 * @param separator 拼接符号
	 * @param array
	 * @return
	 */
	public static String join(final String separator, Object... array) {
		if(array == null || array.length == 0) {
			return StringUtils.EMPTY;
		}
		return StringUtils.join(array, separator);
	}
	
	/**
	 * 拼接list
	 * @param iter 迭代器
	 * @param separator 分隔符
	 * @param callback 回调函数
	 * @return
	 */
	public static <T> String join(final Iterable<T> iter, final String separator, Callback<T, Object> callback) {
		if (iter == null || separator == null) {
            return StringUtils.EMPTY;
        }
		Iterator<T> iterator = iter.iterator();
		if(!iterator.hasNext())
			return StringUtils.EMPTY;
		final T first = iterator.next();
		if(!iterator.hasNext()) {
			return (callback != null ? callback.handle(first) : first).toString();
		}
		final StringBuilder buf = new StringBuilder(256);
		if (first != null) {
            buf.append(callback != null ? callback.handle(first) : first);
        }
		while(iterator.hasNext()) {
	         final T t = iterator.next();
	         if(t != null) {
	        	 buf.append(separator);
	        	 buf.append(callback != null ? callback.handle(t) : t);
	         }
		}
		return buf.toString();
	}
	
	public static <T> String join(final T[] ts, final String separator, Filter<T> filter) {
		if (ts == null || ts.length == 0 || separator == null) {
			return StringUtils.EMPTY;
		}
		int i = 0, length = ts.length;
		final StringBuilder buf = new StringBuilder(ts.length * 16);
        for (; i < length; i++) {
        	T t = ts[i];
            if (filter == null || filter.apply(t)) {
                buf.append(separator).append(t);
            }
        }
        return buf.toString().substring(separator.length());
	}
	
	/**
	 * 字符串追加前后
	 * <pre>
     * appendOpenClose("'", "name1','name2','name3", "'") = 'name1','name2','name3'
     * </pre>
	 */
	public static String appendOpenClose(final String str, final String open, final String close) {
		return (new StringBuilder(open)).append(str).append(close).toString();
	}
	
	/**
	 * 根据参数创建StringBuilder
	 * @param strs
	 * @return
	 */
	public static StringBuilder newBuilder(String... strs) {
		StringBuilder builder = new StringBuilder();
		for(int i=0; strs!=null && i<strs.length; ++i) {
			builder.append(strs[i]);
		}
		return builder;
	}
	
	/**
	 * 是否包含空，如果有一个是空(null、空字符串)则返回true
	 * @param strs
	 * @return
	 */
	public static boolean hasBlank(String... strs) {
		for(int i=0; strs!=null && i<strs.length; ++i) {
			if(StringUtils.isBlank(strs[i]))
				return true;
		}
		return strs==null || strs.length==0;
	}
	
	/**
	 * seq中存在searchSeq返回：defaultStr，否则返回：elseStr
	 * @param seq
	 * @param searchSeq
	 * @param defaultStr
	 * @param elseStr
	 * @return
	 */
	public static String defaultContains(final String seq
			, final String searchSeq
			, final String defaultStr
			, final String elseStr) {
		return StringUtils.contains(seq, searchSeq) ? defaultStr : elseStr;
	}
	
	public static String defaultContains(final String seq
			, final String searchSeq
			, final String defaultStr) {
		return StringUtils.contains(seq, searchSeq) ? defaultStr : StringUtils.EMPTY;
	}
	
}
