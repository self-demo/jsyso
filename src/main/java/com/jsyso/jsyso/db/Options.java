package com.jsyso.jsyso.db;

import java.util.List;

import com.jsyso.jsyso.util.JsMap;

/**
 * 生成sql选项参数对象
 * @author janjan, xujian_jason@163.com
 *
 */
public class Options implements Cloneable {
	private static final int OPTIONS_NUM = 13;
	private JsMap map;
	
	public Options() {
		map = JsMap.create(OPTIONS_NUM);
	}
	
	public Options(JsMap map) {
		this.map = map;
	}
	
	public static Options create() {
		return new Options();
	}
	
	public String getTable() {
		return map.get(Dialect.KEY_TABLE, String.class);
	}
	
	public Options setTable(String tables) {
		map.set(Dialect.KEY_TABLE, tables);
		return this;
	}
	
	public Options setDistinct(Boolean value) {
		map.set(Dialect.KEY_DISTINCT, value);
		return this;
	}
	
	public boolean getDistinct() {
		return map.containsKey(Dialect.KEY_DISTINCT) 
				? map.get(Dialect.KEY_DISTINCT, Boolean.class) : false;
	}
	
	public String getField() {
		return map.get(Dialect.KEY_FIELD, String.class);
	}
	
	public Options setField(String fields) {
		map.set(Dialect.KEY_FIELD, fields);
		return this;
	}
	
	public Options setJoin(List<String> joins) {
		map.set(Dialect.KEY_JOIN, joins);
		return this;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getJoin() {
		return map.get(Dialect.KEY_JOIN, List.class);
	}
	
	public Options setWhere(Wheres value) {
		map.set(Dialect.KEY_WHERE, value);
		return this;
	}
	
	public Wheres getWhere() {
		return map.get(Dialect.KEY_WHERE, Wheres.class);
	}
	
	public Options setGroup(String value) {
		map.set(Dialect.KEY_GROUP, value);
		return this;
	}
	
	public String getGroup() {
		return map.get(Dialect.KEY_GROUP, String.class);
	}
	
	public Options setHaving(String value) {
		map.set(Dialect.KEY_HAVING, value);
		return this;
	}
	
	public String getHaving() {
		return map.get(Dialect.KEY_HAVING, String.class);
	}
	
	public Options setOrder(String value) {
		map.set(Dialect.KEY_ORDER, value);
		return this;
	}
	
	public String getOrder() {
		return map.get(Dialect.KEY_ORDER, String.class);
	}
	
	public Options setUnion(List<Object> value) {
		map.set(Dialect.KEY_UNION, value);
		return this;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getUnion() {
		return map.get(Dialect.KEY_UNION, List.class);
	}
	
	public Options setLock(Boolean value) {
		map.set(Dialect.KEY_LOCK, value);
		return this;
	}
	
	public Boolean getLock() {
		return map.get(Dialect.KEY_LOCK, Boolean.class);
	}
	
	public Options setComment(String value) {
		map.set(Dialect.KEY_COMMENT, value);
		return this;
	}
	
	public String getComment() {
		return map.get(Dialect.KEY_COMMENT, String.class);
	}
	
	public Options setForce(String value) {
		map.set(Dialect.KEY_FORCE, value);
		return this;
	}
	
	public String getForce() {
		return map.get(Dialect.KEY_FORCE, String.class);
	}
	
	public Options setLimit(int[] value) {
		map.set(Dialect.KEY_LIMIT, value);
		return this;
	}
	
	public int[] getLimit() {
		return (int[]) map.get(Dialect.KEY_LIMIT);
	}
	
	public Options setPage(int[] page) {
		map.set(Dialect.KEY_PAGE, page);
		return this;
	}
	
	public int[] getPage() {
		return (int[]) map.get(Dialect.KEY_PAGE);
	}
	
	@Override
	public Object clone() {
		return new Options(new JsMap(map));
	}
}
