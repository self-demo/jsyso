package com.jsyso.jsyso.db;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

/**
 * 数据库
 * @author janjan, xujian_jason@163.com
 *
 */
public class Db {

	// 写库
	private NamedParameterJdbcOperations master;
	// 读库集合
	private NamedParameterJdbcOperations[] slaves;
	// 是否启用读写分类
	private boolean rwSeparate;
	// debug模式
	private boolean debug;
	// 数据库方言
	private Dialect dialect;

	public NamedParameterJdbcOperations getMaster() {
		return master;
	}

	public void setMaster(NamedParameterJdbcOperations master) {
		this.master = master;
	}

	public NamedParameterJdbcOperations[] getSlaves() {
		return slaves;
	}

	public void setSlaves(NamedParameterJdbcOperations[] slaves) {
		this.slaves = slaves;
	}

	public boolean isRwSeparate() {
		return rwSeparate;
	}

	public void setRwSeparate(boolean rwSeparate) {
		this.rwSeparate = rwSeparate;
	}

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public Dialect getDialect() {
		return dialect;
	}

	public void setDialect(Dialect dialect) {
		this.dialect = dialect;
	}
	
	
}
