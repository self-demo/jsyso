package com.jsyso.jsyso.db.dialect;

import org.apache.commons.lang3.StringUtils;

import com.jsyso.jsyso.db.Dialect;

public class MySqlDialect extends Dialect {

	@Override
	public String handleLimit(int[] limits) {
		String limitString = limits == null || limits.length == 0 
					? StringUtils.EMPTY : " LIMIT " + StringUtils.join(limits, ',');
		return limitString;
	}
	
}
