package com.jsyso.jsyso.db.id;

/**
 * 序列生成器
 * @author janjan, xujian_jason@163.com
 *
 */
public interface Sequence {

	/**
	 * 下一个值
	 * @param seqName
	 * @return
	 */
	public Long nextVal(final String seqName);
	
}
