package com.jsyso.jsyso.exception;

/**
 * 转换Exception
 * @author janjan, xujian_jason@163.com
 *
 */
public class CastException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public CastException(){
        super();
    }

    public CastException(String message){
        super(message);
    }

    public CastException(String message, Throwable cause){
        super(message, cause);
    }
}
