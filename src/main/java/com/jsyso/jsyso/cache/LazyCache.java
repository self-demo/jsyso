package com.jsyso.jsyso.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 用懒惰删除方式实现缓存。
 * 缓存对象少可选择此方式，如果对象较多，请勿用！
 * @author janjan, xujian_jason@163.com
 *
 */
public class LazyCache implements Cache {

	private Map<String, Element> cacheContainer = new ConcurrentHashMap<String, Element>();
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T get(Element e) {
		Element element = cacheContainer.get(e.getKey());
		if(element != null && element.isAvailable()) {
			return (T)element.getValue();
		}
		else {
			cacheContainer.remove(e.getKey());
		}
		return null;
	}

	@Override
	public void put(Element e) {
		cacheContainer.put(e.getKey(), e);
	}

	@Override
	public void remove(Element e) {
		cacheContainer.remove(e.getKey());
	}

	@Override
	public void removeAll(Element e) {
		cacheContainer.clear();
	}

}
