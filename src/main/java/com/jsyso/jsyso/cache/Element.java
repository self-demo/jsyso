package com.jsyso.jsyso.cache;

import org.apache.commons.lang3.StringUtils;

/**
 * 缓存参数
 * @author janjan, xujian_jason@163.com
 *
 */
public class Element {

	public static final Element EMPTY = new Element();
	
	private String key;			// 缓存key
	private Object value;		// 缓存value
	private String cacheName;	// EhCache配置的cacheName
	
	private int seconds;		// 超时时间（秒）
	private Long expiredTime;	// 存放过期时间（毫秒数）

	public static Element create() {
		return new Element();
	}
	
	public static Element create(String key) {
		return new Element().setKey(key);
	}
	
	public static Element create(String key, Object value) {
		return new Element().setKey(key).setValue(value);
	}
	
	public String getKey() {
		return key;
	}

	public Element setKey(String key) {
		this.key = key;
		return this;
	}

	public Object getValue() {
		return value;
	}

	public Element setValue(Object value) {
		this.value = value;
		return this;
	}
	
	public String getCacheName() {
		return cacheName;
	}

	public Element setCacheName(String cacheName) {
		this.cacheName = cacheName;
		return this;
	}

	public boolean keyIsBlank() {
		return StringUtils.isBlank(this.key);
	}
	
	public boolean cacheNameIsBlank() {
		return StringUtils.isBlank(this.cacheName);
	}

	public int getSeconds() {
		return seconds;
	}

	public Element setSeconds(int seconds) {
		this.seconds = seconds;
		if(seconds > 0)
			expiredTime = System.currentTimeMillis() + (seconds * 1000);
		return this;
	}
	
	public Long getExpiredTime() {
		return expiredTime;
	}
	
	/**
	 * 当前元素是可用的
	 */
	public boolean isAvailable() {
		return expiredTime != null && expiredTime.longValue() >= System.currentTimeMillis();
    }

}
