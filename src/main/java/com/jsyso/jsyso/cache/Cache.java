package com.jsyso.jsyso.cache;

/**
 * 缓存接口
 * @author janjan, xujian_jason@163.com
 *
 */
public interface Cache {
	/**
	 * 从缓存中获取
	 */
	<T> T get(Element e);
	
	/**
	 * 存入缓存
	 */
	void put(Element e);
	
	/**
	 * 删除缓存
	 * @param param
	 */
	void remove(Element e);
	
	/**
	 * 删除所有
	 * @param param
	 */
	void removeAll(Element e);
}
