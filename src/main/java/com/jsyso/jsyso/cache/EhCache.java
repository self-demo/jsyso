package com.jsyso.jsyso.cache;

/**
 * EhCache缓存实现
 * @author janjan, xujian_jason@163.com
 *
 */
public class EhCache implements Cache {

	private net.sf.ehcache.CacheManager cacheManager;
	
	public void setCacheManager(net.sf.ehcache.CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T get(Element e) {
		if(e == null || e.keyIsBlank() 
				|| e.cacheNameIsBlank()) {
			return null;
		}
		net.sf.ehcache.Element element = getCache(e.getCacheName()).get(e.getKey());
		return element == null ? null : (T) element.getObjectValue();
	}

	@Override
	public void put(Element e) {
		if(e == null || e.keyIsBlank() 
				|| e.cacheNameIsBlank()) {
			return ;
		}
		net.sf.ehcache.Element element = new net.sf.ehcache.Element(e.getKey(), e.getValue());
		getCache(e.getCacheName()).put(element);
	}

	@Override
	public void remove(Element e) {
		if(e == null || e.keyIsBlank() 
				|| e.cacheNameIsBlank()) {
			return ;
		}
		getCache(e.getCacheName()).remove(e.getKey());
	}

	@Override
	public void removeAll(Element e) {
		if(e == null || e.cacheNameIsBlank()) {
			return ;
		}
		getCache(e.getCacheName()).removeAll();
	}
	
	/**
	 * 获得一个Cache，没有则创建一个。
	 * @return
	 */
	protected net.sf.ehcache.Cache getCache(String cacheName) {
		if(cacheManager == null) {
			throw new RuntimeException("Spring未注入EhCacheManager");
		}
		net.sf.ehcache.Cache cache = cacheManager.getCache(cacheName);
		if (cache == null){
			cacheManager.addCache(cacheName);
			cache = cacheManager.getCache(cacheName);
			cache.getCacheConfiguration().setEternal(true);
		}
		return cache;
	}

}
