package com.jsyso.jsyso.util.jcollect;

import java.util.Collection;
import java.util.Map;

/**
 * 集合框架过滤工具类
 * @author janjan, xujian_jason@163.com
 *
 */
public final class JCollect {

	/**
	 * 开始map
	 * @param map 传入map对象
	 * @return org.jwxa.jsyso.util.jcollect.JMap<K, V>
	 */
	public static final <K, V> JMap<K, V> begin(Map<K, V> map) {
		return new JMap<K, V>(map);
	}
	
	/**
	 * 开始list
	 * @param map 传入map对象
	 * @return org.jwxa.jsyso.util.jcollect.JMap<K, V>
	 */
	public static final <E> JList<E> begin(Collection<E> list) {
		return new JList<E>(list);
	}
	
}
