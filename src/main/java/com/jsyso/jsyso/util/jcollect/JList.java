package com.jsyso.jsyso.util.jcollect;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 集合工具
 * @author janjan, xujian_jason@163.com
 *
 * @param <E>
 */
public class JList<E> {

	private Collection<E> list;
	
	@SuppressWarnings("unchecked")
	public JList(Collection<E> list) {
		if(list != null) {
			try {
				this.list = list.getClass().newInstance();
			} catch (Exception e) {
				this.list = new ArrayList<E>();
			}
			this.list.addAll(list);
		}
	}
	
	public boolean isEmpty() {
		return list == null || list.isEmpty();
	}
	
}
