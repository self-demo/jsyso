package com.jsyso.jsyso.util;

public interface Filter<T> {

	boolean apply(T t);
	
}
