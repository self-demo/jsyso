package com.jsyso.jsyso.util;

/**
 * 回调
 * @author janjan, xujian_jason@163.com
 *
 * @param <P> 参数
 * @param <R> 返回
 */
public interface Callback<P, R> {

	/**
	 * 处理回调
	 * @param param
	 * @return
	 */
	R handle(P param);
	
}
