package com.jsyso.jsyso.util;

/**
 * 处理器
 * @author janjan, xujian_jason@163.com
 *
 * @param <P>
 */
public abstract class AbsHandler<P> implements Callback<P, Object> {

	@Override
	public Object handle(P param) {
		doHandle(param);
		return null;
	}
	
	public abstract void doHandle(P param);
	
}
