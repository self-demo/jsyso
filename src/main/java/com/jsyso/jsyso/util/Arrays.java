package com.jsyso.jsyso.util;

import java.util.List;

/**
 * 数组工具类
 * @author janjan
 *
 */
public abstract class Arrays {
	
	/**
	 * 从数组中获取value
	 * @param ts 数据
	 * @param index 索引
	 * @param defaultValue 默认值
	 * @return
	 */
	public static int get(int[] ts, int index, int defaultValue) {
		if(ts == null || index < 0 || ts.length <= index) {
			return defaultValue;
		}
		return ts[index];
	}
	
	/**
	 * 从数组中随机获取value
	 * @param ts
	 * @return
	 */
	public static <T> T getRandom(T[] ts, T defaultValue) {
		if(ts == null || ts.length <= 0) {
			return null;
		}
		T t = ts[((int) (Math.random() * ts.length))];
		return t == null ? defaultValue : t;
	}
	
	/**
	 * 新建数组
	 * @param ts
	 * @return
	 */
	public static <T> T[] news(T... ts) {
		return ts;
	}
	
	/**
	 * 遍历list
	 */
	public static <E> void each(List<E> list, AbsHandler<E> handle) {
		if(list != null && list.size() > 0) {
			for(E e : list) {
				handle.doHandle(e);
			}
		}
	}
	
}
