package com.jsyso.jsyso.web;

import com.jsyso.jsyso.util.JsMap;

/**
 * JsMap Web参数
 * @author janjan, xujian_jason@163.com
 *
 */
public class JsMapParam {

	public static JsMapParam create(JsMap data) {
		JsMapParam mapParam = new JsMapParam();
		mapParam.setData(data);
		return mapParam;
	}
	
	private JsMap data;

	public JsMap getData() {
		return data;
	}

	public void setData(JsMap data) {
		this.data = data;
	}
	
	@Override
	public String toString() {
		return data.toString();
	}
	
}
