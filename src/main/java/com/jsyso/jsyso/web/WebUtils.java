package com.jsyso.jsyso.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import com.jsyso.jsyso.json.jackson.JsonMapper;
import com.jsyso.jsyso.util.CastUtils;

/**
 * web工具类
 * @author janjan, xujian_jason@163.com
 *
 */
public abstract class WebUtils {
	
	public static String getParam(ServletRequest request, String name, String defaultValue) {
		return StringUtils.defaultString(request.getParameter(name), defaultValue);
	}
	
	public static <T> T getParam(ServletRequest request, String name, T defaultValue, Class<T> clazz) {
		String value = request.getParameter(name);
		T t = defaultValue;
		try {
			t = CastUtils.cast(value, clazz);
		} catch (Exception e) {
		}
		return t == null ? defaultValue : t;
	}

	public static String renderString(ServletResponse response, Object object) {
		return renderString(response, JsonMapper.toJsonString(object), "application/json");
	}
	
	public static String renderString(ServletResponse response, String string, String type) {
		try {
			response.reset();
	        response.setContentType(type);
	        response.setCharacterEncoding("utf-8");
			response.getWriter().print(string);
			return null;
		} catch (IOException e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T[] getParamValues(ServletRequest request, String name, Class<T> clazz) {
		String[] strValues = request.getParameterValues(name);
		if(strValues == null) 
			return null;
		List<T> listValues = new ArrayList<T>(strValues.length);
		int i=0, length=strValues.length;
		for(; i<length; ++i) 
			listValues.add(CastUtils.cast(strValues[i], clazz));
		return (T[]) listValues.toArray();
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getSessionAttr(HttpServletRequest request, String key) {
		HttpSession session = request.getSession(false);
		return session != null ? (T)session.getAttribute(key) : null;
	}
	
	public static boolean sessionHasKey(HttpServletRequest request, String key) {
		Object value = getSessionAttr(request, key);
		return value != null;
	}
	
	public static void setSessionAttr(HttpServletRequest request,String key, Object value) {
		request.getSession(true).setAttribute(key, value);
	}
	
	public static void removeSessionAttr(HttpServletRequest request, String key) {
		HttpSession session = request.getSession(false);
		if (session != null)
			session.removeAttribute(key);
	}
	
	public static String getRemoteAddr(HttpServletRequest request){
		String remoteAddr = request.getHeader("X-Real-IP");
        if (StringUtils.isNotBlank(remoteAddr)) {
        	remoteAddr = request.getHeader("X-Forwarded-For");
        }else if (StringUtils.isNotBlank(remoteAddr)) {
        	remoteAddr = request.getHeader("Proxy-Client-IP");
        }else if (StringUtils.isNotBlank(remoteAddr)) {
        	remoteAddr = request.getHeader("WL-Proxy-Client-IP");
        }
        return remoteAddr != null ? remoteAddr : request.getRemoteAddr();
	}
	
}
